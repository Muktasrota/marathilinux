---
layout: default
---

**सोपे लिनक्स मार्गदर्शक** हे पुस्तक, संगणक तंत्रज्ञानाशी संबंधीत नसलेल्या वापरकर्त्यांना लिनक्स शिकताना उपयोगी पडावे या हेतुने तयार केलेले आहे. हे पुस्तक मुक्तस्त्रोत असुन क्रियेटीव्ह कॉमन्सच्या अ‍ॅट्रिब्युशन शेअर अलाईक ४.० या परवान्यांतर्गत प्रसिद्ध केले आहे.

This book is written in Marathi Language to help readers learn how to use Linux OS for day to day tasks. It is released as per Creative Commons Attribution ShareAlike 4.0 License.

## हे पुस्तक पुर्ण वाचुन झाल्यानंतर तुम्ही काय शिकलेले असाल?

* मुक्तस्त्रोत म्हणजे काय?
* ओएस(O.S.), डी.ई.(D.E.) व डिस्ट्रो(Distro) म्हणजे काय?
* लिनक्स ओ.एस. कशी ईनस्टॊल करावी?
* लिनक्स ओ.एस. वर प्रिंटर वगैरे हार्डवेअर कसे सेटअप करावे?
* दैनंदीन कार्यालयीन व खाजगी काम लिनक्सवर कसे करावे?

## पुस्तक
पुस्तकाचे काम अजुन चालु आहे. लवकरच तुम्हाला हे पुस्तक पिडीएफ स्वरुपात उतरवुन घेता येईल.

## मदत
पुस्तक वाचताना तुम्हाला जर लिनक्स संबंधी काही मदत लागली तर, खाली दिलेल्या ब्लॉगची संपर्क सुविधा वापरुन प्रश्न विचारु शकता.

## स्रोत भांडार
हे मुक्तस्त्रोत पुस्त्तक, टेक्स(Tex) वापरुन लिहिलेले आहे. त्याचा स्त्रोत बघण्यासाठी [इथे](https://gitlab.com/Muktasrota/marathilinux) क्लिक करा.

## लेखकाबद्दल
सदर पुस्तक श्री. अभिजित नवले यांनी तयार केले आहे. अभिजित स्वतः एक संगणक अभियंता आहेत.  

संकेतस्थळः https://abhijitnavale.com/  
ब्लॉग: https://blog.abhijitnavale.com/

---
## Credits

This static web page is created with [Solo Theme](https://github.com/chibicode/solo/),  
using [Jekyll](https://jekyllrb.com/) Static Site Generator and  
hosted on [Gitlab.com](https://gitlab.com/).
