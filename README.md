# Linux QuickStart - Maraathi
E-Book to learn Gnu/Linux OS in Maraathi.

# ToDos
1. Add LMM Topic

# Software Requirements
1. XeLaTex
2. Ubuntu

# How To Setup
1. If `~/texmf` does not exist already then, do `mkdir ~/texmf`  
2. Run setup_ccicons
mv tex to texmf
texhash texmf
https://www.ctan.org/pkg/ccicons

# License

## License for the E-Book, Text and Images
Copyright © Abhijit Navale 2017  
contact@abhijitnavale.com

[<img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" />](http://creativecommons.org/licenses/by-sa/4.0/)

MarathiLinux E-Book is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](http://creativecommons.org/licenses/by-sa/4.0/).

You are free to:

Share — copy and redistribute the material in any medium or format  
Adapt — remix, transform, and build upon the material  
for any purpose, even commercially.  

**Under the following terms:**

Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.

ShareAlike — If you remix, transform, or build upon the material, you must distribute your contributions under the same license as the original.

## License for the software code

Copyright (C) 2017  Abhijit Navale  
contact@abhijitnavale.com

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
