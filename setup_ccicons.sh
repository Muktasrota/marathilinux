#!/bin/bash

mkdir -p doc/latex/ccicons
mkdir -p fonts/enc/dvips/ccicons
mkdir -p fonts/map/dvips/ccicons
mkdir -p fonts/tfm/public/ccicons
mkdir -p fonts/type1/public/ccicons
mkdir -p fonts/opentype/public/ccicons
mkdir -p tex/latex/ccicons

cp ccicons/ccicons.pdf doc/latex/ccicons/
cp ccicons/ccicons-u.enc fonts/enc/dvips/ccicons/
cp ccicons/ccicons.map fonts/map/dvips/ccicons/
cp ccicons/ccicons.tfm fonts/tfm/public/ccicons/
cp ccicons/ccicons.pfb fonts/type1/public/ccicons/
cp ccicons/ccicons.otf fonts/opentype/public/ccicons/
cp ccicons/ccicons.sty tex/latex/ccicons/
